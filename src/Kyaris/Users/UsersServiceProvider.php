<?php namespace Kyaris\Users;

use Illuminate\Support\ServiceProvider;
use Sentry;
use View;

class UsersServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// override the default platform user and group models
		$this->app['Platform\Users\Models\Group'] = new \Kyaris\Users\Models\GroupModel;
		$this->app['Platform\Users\Models\User']  = new \Kyaris\Users\Models\UserModel;
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
