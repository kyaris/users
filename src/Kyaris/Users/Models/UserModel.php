<?php namespace Kyaris\Users\Models;
/**
 * Part of the Kyaris application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Kyaris
 * @version    1.0.0
 * @author     Kyaris
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2014, Kyaris LLC
 * @link       http://kyaris.com
 */

use Platform\Users\Models\User;

class UserModel extends User {}
